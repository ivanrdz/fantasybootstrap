export interface userInterface {
    idUser: number;
    userUser: any;
    passwordUser: any;
    lastnameUser?: string;
    nameUser: string;
    ageUser?: number;
    emailUser: any;
    fanOfUser?: string;
    cellphoneUser?: number;
}