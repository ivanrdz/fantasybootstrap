export interface playerInterface {
    idPlayer: number;
    teamPlayer: string;
    namePlayer: string;
    lastnamePlayer: string;
    nicknamePlayer: string;
    jerseyPlayer: number;
    positionPlayer: number;
    pointsPlayer: number;
    redcardPlayer: number;
    yellowcardPlayer: number;
    goalsPlayer: number;
    levelPlayer: number;
    dateLastModifPlayer: Date;
    agePlayer: number;
    nationalityPlayer: string;
}