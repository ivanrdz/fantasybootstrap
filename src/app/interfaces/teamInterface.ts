export interface teamInterface {
    idTeam: number;
    teamTeam: string;
    playersOnTeam: string;
    pointsOnLeagueTeam: number;
    positionsTeam: number;
    goalsTeam: number;
    redcardsTeam: number;
    yellowcardsTeam: number;
    dateLastModifTeam: Date;
}