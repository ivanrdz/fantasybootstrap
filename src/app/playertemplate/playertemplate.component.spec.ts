import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayertemplateComponent } from './playertemplate.component';

describe('PlayertemplateComponent', () => {
  let component: PlayertemplateComponent;
  let fixture: ComponentFixture<PlayertemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayertemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayertemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
